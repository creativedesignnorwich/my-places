var gulp            = require('gulp');
var less            = require('gulp-less');
var minifycss       = require('gulp-minify-css');
var autoprefixer    = require('gulp-autoprefixer');
var uglify          = require('gulp-uglify');
var concat          = require('gulp-concat');

gulp.task('appCssFrontend', function() {
 gulp.src('resources/assets/frontend/less/app.less')
     .pipe(less())
     .pipe(autoprefixer('last 10 version'))
     .pipe(minifycss())
     .pipe(gulp.dest('public'));
});

gulp.task('appJs', function() {
    gulp.src([
    	'resources/assets/frontend/js/jquery.min.js',
        'resources/assets/frontend/js/bootstrap.min.js',
        'resources/assets/frontend/js/lightbox.min.js',
        'resources/assets/frontend/js/owl.carousel.min.js',
        'resources/assets/frontend/js/jquery.countTo.js',
        'resources/assets/frontend/js/jquery.appear.js',
        'resources/assets/frontend/js/SmoothScroll.js',
        'resources/assets/frontend/js/isotope.pkgd.min.js',
        'resources/assets/frontend/js/jquery.localScroll.min.js',
        'resources/assets/frontend/js/jquery.scrollTo.min.js',
        'resources/assets/frontend/js/jquery.simple-text-rotator.min.js',
        'resources/assets/frontend/js/jquery.easing.min.js',
        'resources/assets/frontend/js/imagesloaded.pkgd.min.js',
        'resources/assets/frontend/js/wow.min.js',
        'resources/assets/frontend/js/jquery.themepunch.tools.min.js',
        'resources/assets/frontend/js/jquery.themepunch.revolution.min.js',
        'resources/assets/frontend/js/rev-slider.js',
        'resources/assets/frontend/js/scripts.js',
        'resources/assets/frontend/js/revolution.extension.video.min.js',
        'resources/assets/frontend/js/revolution.extension.carousel.min.js',
        'resources/assets/frontend/js/revolution.extension.slideanims.min.js',
        'resources/assets/frontend/js/revolution.extension.actions.min.js',
        'resources/assets/frontend/js/revolution.extension.layeranimation.min.js',
        'resources/assets/frontend/js/revolution.extension.kenburn.min.js',
        'resources/assets/frontend/js/revolution.extension.navigation.min.js',
        'resources/assets/frontend/js/revolution.extension.migration.min.js',
        'resources/assets/frontend/js/revolution.extension.parallax.min.js'
        ])
        .pipe(concat('app.js'))
        .pipe(gulp.dest('public'));
});

gulp.task('watchCss', function() {
 gulp.watch('resources/assets/frontend/less/**/*.less', ['appCssFrontend']);
});

gulp.task('watchJs', function() {
 gulp.watch('resources/assets/frontend/js/**/*.js', ['appJs']);
});

gulp.task('default', ['watchCss', 'watchJs']);