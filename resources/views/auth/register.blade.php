@extends('layouts.auth')

@section('content')

<div class="panel panel-color panel-primary panel-pages">
    <div class="panel-heading bg-img"> 
        <div class="bg-overlay"></div>
       <h3 class="text-center m-t-10 text-white"><img src="/upload/logo-light.png" alt="logo" width="150px"></h3>
    </div> 


    <div class="panel-body">

        <form class="form-horizontal m-t-20" role="form" method="POST" action="{{ url('/register') }}">
            {!! csrf_field() !!}

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <div class="col-xs-12">
                    <input class="form-control input-lg" type="text" required="" placeholder="Name" name="name" value="{{ old('name') }}">
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <div class="col-xs-12">
                    <input class="form-control input-lg" type="email" name="email" required="" placeholder="Email" value="{{ old('email') }}">
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <div class="col-xs-12">
                    <input class="form-control input-lg" type="password" name="password" required="" placeholder="Password">
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <div class="col-xs-12">
                    <input class="form-control input-lg" type="password" name="password_confirmation" required="" placeholder="Confirm Password">
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- <div class="form-group">
                <div class="col-xs-12">
                    <div class="checkbox checkbox-primary">
                        <input id="checkbox-signup" type="checkbox" checked="checked">
                        <label for="checkbox-signup">
                            I accept <a href="">Terms and Conditions</a>
                        </label>
                    </div>
                    
                </div>
            </div> -->
            
            <div class="form-group text-center m-t-40">
                <div class="col-xs-12">
                    <button class="btn btn-primary waves-effect waves-light btn-lg w-lg" type="submit">Register</button>
                </div>
            </div>

            <div class="form-group m-t-30">
                <div class="col-sm-12 text-center">
                    <a href="{{ url('/login') }}">Already have account?</a>
                </div>
            </div>
        </form> 

    </div>                                 
    
</div>
@endsection
