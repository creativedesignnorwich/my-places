<!DOCTYPE html>
<html lang="en">
    <head>
        <title>My Places</title>
        <meta name="description" content="My Places">
        <meta charset="utf-8">
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <link href='http://fonts.googleapis.com/css?family=Lato:300italic,400italic,600italic,700italic,800italic,400,800,700,600,300' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Roboto:300,700,400' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/app.css" />
        <link rel='shortcut icon' type='image/x-icon' href='/fav.ico' />
    </head>

    <body data-spy="scroll" data-offset="80" data-target=".main-nav">

        @include('partials.loader')

        @include('partials.header')

        <div class="main-wrapper-onepage oh">

            @include('partials.slider')

            @include('partials.intro')

            @include('partials.statistics')

            @include('partials.portfolio')

            @include('partials.call-to-action')

            @include('partials.about')

            @include('partials.services')

            @include('partials.testimonials')

            @include('partials.blog')

            @include('partials.pricing')

            @include('partials.partners')

            @include('partials.contact')

            @include('partials.footer') 

            <div id="back-to-top">
                <a href="#top"><i class="arrow_carrot-up"></i></a>
            </div>

        </div> 
        
        <script type="text/javascript" src="/app.js"></script>

    </body>
</html>