<section class="section-wrap icon-boxes" id="services">
    <div class="container">
        <h2 class="text-center">Our Services</h2>
        <p class="subheading text-center">That's What We Do</p>

        <div class="row">

            <div class="col-md-3 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.2s">
                <div class="service-item-box mt-40 text-center icon-effect-1 icon-effect-1a">
                    <a href="#">
                        <i class="hi-icon icon_desktop"></i>
                    </a>
                    <h3>Web Design</h3>
                    <p class="mb-0">We possess within us two minds. So far I have written only of the conscious mind.
                    Check our services.</p>         
                </div>                      
            </div> 

            <div class="col-md-3 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.4s">
                <div class="service-item-box mt-40 text-center icon-effect-1 icon-effect-1a">
                    <a href="#">
                        <i class="hi-icon icon_mail_alt"></i>
                    </a>
                    <h3>E-mail Marketing</h3>
                    <p class="mb-0">We possess within us two minds. So far I have written only of the conscious mind.
                    Check our services.</p>     
                </div>                      
            </div> 

            <div class="col-md-3 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.6s">
                <div class="service-item-box mt-40 text-center icon-effect-1 icon-effect-1a">
                    <a href="#">
                        <i class="hi-icon icon_tablet"></i>
                    </a>
                    <h3>App Development</h3>
                    <p class="mb-0">We possess within us two minds. So far I have written only of the conscious mind.
                    Check our services.</p>         
                </div>                      
            </div> 

            <div class="col-md-3 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.8s">
                <div class="service-item-box mt-40 text-center icon-effect-1 icon-effect-1a">
                    <a href="#">
                        <i class="hi-icon icon_desktop"></i>
                    </a>
                    <h3>Web Design</h3>
                    <p class="mb-0">We possess within us two minds. So far I have written only of the conscious mind.
                    Check our services.</p>         
                </div>                      
            </div> 
            
        </div> 
    </div> 
</section>