<section class="results bg-color pt-mdm-80 pb-mdm-80">
    <div class="container">
        <div class="row">

            <div class="col-md-3 text-center">
                <div class="statistic">
                    <span class="timer" data-from="0" data-to="968">&nbsp;</span>
                    <h5 class="counter-text color-white">Property Managers</h5>
                </div>
            </div> 

            <div class="col-md-3 text-center">
                <div class="statistic mt-mdm-40">
                    <span class="timer" data-from="0" data-to="357">&nbsp;</span>
                    <h5 class="counter-text color-white">Properties Active</h5>
                </div>
            </div> 

            <div class="col-md-3 text-center">
                <div class="statistic mt-mdm-40">
                    <span class="timer" data-from="0" data-to="813">&nbsp;</span>
                    <h5 class="counter-text color-white">Properties Sold</h5>
                </div>
            </div> 

            <div class="col-md-3 text-center">
                <div class="statistic mt-mdm-40">
                    <span class="timer" data-from="0" data-to="972">&nbsp;</span>
                    <h5 class="counter-text color-white">Properties Developed</h5>
                </div>
            </div> 

        </div>
    </div> 
</section> 