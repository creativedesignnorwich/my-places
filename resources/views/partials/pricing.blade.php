<section class="section-wrap bg-color pricing-tables" id="pricing">
    <div class="container">
        <h2 class="text-center color-white">Best Prices</h2>
        <p class="subheading color-white text-center">Our Affordable Prices</p>

        <div class="row mt-20">

            <div class="col-lg-3 col-md-6">             
                <div class="pricing-4-col text-center mt-30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.2s">
                    <div class="pricing-title">
                        <h3>Basic</h3>
                        <div class="pricing-price">
                            <span class="pricing-currency">$</span>
                            <span class="price">72</span>
                            <span class="pricing-term">Monthly</span>
                        </div>
                        
                    </div>

                    <div class="pricing-features mt-30">
                        <ul>
                            <li>Fully Responsive</li>
                            <li>Clean Design</li>
                            <li>Tons of Features</li>
                        </ul>
                    </div>

                    <div class="pricing-button mt-30">
                        <a href="#">
                            <div class="btn btn-medium">Order Now</div>
                        </a>
                    </div>
                </div> 
            </div> 

            <div class="col-lg-3 col-md-6">             
                <div class="pricing-4-col text-center mt-30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="pricing-title">
                        <h3>Business</h3>
                        <div class="pricing-price">
                            <span class="pricing-currency">$</span>
                            <span class="price">129</span>
                            <span class="pricing-term">Monthly</span>
                        </div>
                        
                    </div>

                    <div class="pricing-features mt-30">
                        <ul>
                            <li>Fully Responsive</li>
                            <li>Clean Design</li>
                            <li>Tons of Features</li>
                        </ul>
                    </div>

                    <div class="pricing-button mt-30">
                        <a href="#">
                            <div class="btn btn-medium">Order Now</div>
                        </a>
                    </div>
                </div> 
            </div> 

            <div class="col-lg-3 col-md-6">             
                <div class="pricing-4-col best-price text-center mt-30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.4s">
                    <div class="pricing-title">
                        <h3>Advanced</h3>
                        <div class="pricing-price">
                            <span class="pricing-currency">$</span>
                            <span class="price">299</span>
                            <span class="pricing-term">Monthly</span>
                        </div>
                        
                    </div>

                    <div class="pricing-features mt-30">
                        <ul>
                            <li>Fully Responsive</li>
                            <li>Clean Design</li>
                            <li>Tons of Features</li>
                        </ul>
                    </div>

                    <div class="pricing-button mt-30">
                        <a href="#">
                            <div class="btn btn-medium btn-pink">Order Now</div>
                        </a>
                    </div>
                </div> 
            </div> 

            
            <div class="col-lg-3 col-md-6">             
                <div class="pricing-4-col text-center mt-30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
                    <div class="pricing-title">
                        <h3>Premium</h3>
                        <div class="pricing-price">
                            <span class="pricing-currency">$</span>
                            <span class="price">359</span>
                            <span class="pricing-term">Monthly</span>
                        </div>
                        
                    </div>

                    <div class="pricing-features mt-30">
                        <ul>
                            <li>Fully Responsive</li>
                            <li>Clean Design</li>
                            <li>Tons of Features</li>
                        </ul>
                    </div>

                    <div class="pricing-button mt-30">
                        <a href="#">
                            <div class="btn btn-medium">Order Now</div>
                        </a>
                    </div>
                </div> 
            </div> 

        </div> 
    </div> 
</section> 