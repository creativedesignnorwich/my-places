<section class="section-wrap testimonials-slider relative">

    <div class="overlay-bg">&nbsp;</div>

    <div class="container relative">
        <h2 class="color-white  text-center">What Our Clients Say</h2>
        <p class="subheading color-white text-center">They Are Proud To Work With Us</p>

        <div class="row">
            <div id="owl-testimonials" class="owl-carousel owl-theme mt-40">

                <div class="item">
                    <div class="testimonials-box">
                        <img src="upload/testimonial_1.jpg" class="testimonials-img" alt="">
                        <div class="testimonial-details">
                            <p>Thank for such a great template. Quality Theme is awesome. So amazing features and you can create any website with this perfect solution</p>
                            <h5 class="testimonial-name">John Doe</h5>
                            <span> CEO of Company</span>
                        </div>
                    </div>
                </div> 

                <div class="item">
                    <div class="testimonials-box">
                        <img src="upload/testimonial_2.jpg" class="testimonials-img" alt="">
                        <div class="testimonial-details">
                            <p>Thank for such a great template. Quality Theme is awesome. So amazing features and you can create any website with this perfect solution</p>
                            <h5 class="testimonial-name">John Doe</h5>
                            <span> CEO of Company</span>
                        </div>
                    </div>
                </div> 

                <div class="item">
                    <div class="testimonials-box">
                        <img src="upload/testimonial_1.jpg" class="testimonials-img" alt="">
                        <div class="testimonial-details">
                            <p>Thank for such a great template. Quality Theme is awesome. So amazing features and you can create any website with this perfect solution</p>
                            <h5 class="testimonial-name">John Doe</h5>
                            <span> CEO of Company</span>
                        </div>
                    </div>
                </div> 

                <div class="item">
                    <div class="testimonials-box">
                        <img src="upload/testimonial_2.jpg" class="testimonials-img" alt="">
                        <div class="testimonial-details">
                            <p>Thank for such a great template. Quality Theme is awesome. So amazing features and you can create any website with this perfect solution</p>
                            <h5 class="testimonial-name">John Doe</h5>
                            <span> CEO of Company</span>
                        </div>
                    </div>
                </div> 

            </div> 

            <div class="customNavigation text-center mt-50">
                <a class="btn prev"><i class="arrow_carrot-left"></i></a>
                <a class="btn next"><i class="arrow_carrot-right"></i></a>
            </div>

        </div>               
    </div> 
</section> 