<section class="section-wrap intro" id="intro">
    <div class="container">
        <div class="row">

            <div class="col-sm-10 col-sm-offset-1 text-center wow slideInUp" data-wow-duration="1.2s" data-wow-delay="0s">
                <h2 class="intro-heading">
                We Provide Smarter Property Management<br>
                For All Sized Businesses 
                </h2>
                <p class="intro-text">
                    My Places is a smart and efficient software platform for small and large businesses to control there properties saving them time and money.
                </p>
            </div>

        </div> 
    </div> 
</section> 