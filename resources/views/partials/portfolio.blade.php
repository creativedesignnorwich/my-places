<section class="section-wrap" id="portfolio-masonry">
	<div class="container">
	    <h2 class="text-center">Popular Properties</h2>
	    <p class="subheading text-center">Creative and Modern</p>

	    <div class="row mt-50">
	        <div class="col-md-12">
	            <div class="portfolio-filter">
	                <a href="#" class="filter active transition" data-filter="*">All</a>
	                <a href="#" class="filter transition" data-filter=".branding">Houses</a>
	                <a href="#" class="filter transition" data-filter=".illustration">Flats / Apartments</a>
	                <a href="#" class="filter transition" data-filter=".web-design">Bungalows</a>
	                <a href="#" class="filter transition" data-filter=".print">Land</a>
	            </div> 
	        </div> 
	    </div> 

	    <div class="grid masonry">

	        <div class="masonry-item web-design print">
	            <div class="work-img">
	                <a href="upload/project_1_big.jpg" data-lightbox="true">
	                    <img src="upload/project_1.jpg" alt="img">                          
	                    <div class="work-description text-center">
	                        <h3>A4 Paper Mockup</h3>
	                        <span>Print</span>
	                    </div>
	                </a>
	            </div>
	        </div> 

	        <div class="masonry-item web-design branding">
	            <div class="work-img">
	                <a href="upload/project_2_big.jpg" data-lightbox="true">
	                    <img src="upload/project_2.jpg" alt="img">                          
	                    <div class="work-description text-center">
	                        <h3>Vinyl Cover</h3>
	                        <span>Branding</span>
	                    </div>
	                </a>
	            </div>
	        </div> 

	        <div class="masonry-item illustration branding">
	            <div class="work-img">
	                <a href="upload/project_3_big.jpg" data-lightbox="true">
	                    <img src="upload/project_3.jpg" alt="img">                          
	                    <div class="work-description text-center">
	                        <h3>Paper Bag</h3>
	                        <span>Illustration</span>
	                    </div>
	                </a>
	            </div>
	        </div> 

	        <div class="masonry-item print branding">
	            <div class="work-img">
	                <a href="upload/project_4_big.jpg" data-lightbox="true">
	                    <img src="upload/project_4.jpg" alt="img">                          
	                    <div class="work-description text-center">
	                        <h3>Business Card</h3>
	                        <span>Branding</span>
	                    </div>
	                </a>
	            </div>
	        </div> 

	        <div class="masonry-item branding illustration">
	            <div class="work-img">
	                <a href="upload/project_5_big.jpg" data-lightbox="true">
	                    <img src="upload/project_5.jpg" alt="img">                          
	                    <div class="work-description text-center">
	                        <h3>Sweater Identity</h3>
	                        <span>Branding</span>
	                    </div>
	                </a>
	            </div>
	        </div> 

	        <div class="masonry-item print branding">
	            <div class="work-img">
	                <a href="upload/project_6_big.jpg" data-lightbox="true">
	                    <img src="upload/project_6.jpg" alt="img">                          
	                    <div class="work-description text-center">
	                        <h3>Business Card</h3>
	                        <span>Print</span>
	                    </div>
	                </a>
	            </div>
	        </div> 

	        <div class="masonry-item print illustration">
	            <div class="work-img">
	                <a href="upload/project_7_big.jpg" data-lightbox="true">
	                    <img src="upload/project_7.jpg" alt="img">                          
	                    <div class="work-description text-center">
	                        <h3>Cool Poster</h3>
	                        <span>Print</span>
	                    </div>
	                </a>
	            </div>
	        </div> 

	        <div class="masonry-item web-design">
	            <div class="work-img">
	                <a href="upload/project_8_big.jpg" data-lightbox="true">
	                    <img src="upload/project_8.jpg" alt="img">                          
	                    <div class="work-description text-center">
	                        <h3>Gravity Paper</h3>
	                        <span>Web Design</span>
	                    </div>
	                </a>
	            </div>
	        </div> 

	        <div class="masonry-item illustration">
	            <div class="work-img">
	                <a href="upload/project_9_big.jpg" data-lightbox="true">
	                    <img src="upload/project_9.jpg" alt="img">                          
	                    <div class="work-description text-center">
	                        <h3>Business Card</h3>
	                        <span>Illustration</span>
	                    </div>
	                </a>
	            </div>
	        </div> 

	    </div>            
	</div> 
</section> 