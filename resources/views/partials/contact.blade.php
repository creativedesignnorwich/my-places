<section class="section-wrap contact" id="contact">
    <div class="container">
        <h2 class="text-center">Drop Us a Line</h2>
        <p class="subheading text-center">We are happy to talk with you.</p>

        <div class="row mt-50">

            <div class="col-sm-7 wow bounceInLeft" data-wow-duration="2s" data-wow-delay="0.2s">
                <form id="contact-form" action="#">
                    <input name="name" id="name" type="text" class="form-control" placeholder="Name*">
                    <input name="mail" id="mail" type="email" class="form-control" placeholder="E-mail*">
                    <textarea name="comment" id="comment" class="form-control" placeholder="Message"></textarea>
                    <input type="submit" class="btn btn-pink btn-large btn-submit" value="Send Message" id="submit-message">            
                    <div id="msg" class="message"></div>
                </form>
            </div>

            <div class="col-sm-4 col-sm-offset-1 wow bounceInRight" data-wow-duration="2s" data-wow-delay="0.2s">
                <div class="contact-details mt-sml-50">

                    <h4>Get in Touch</h4>
                    <p>Monday-Friday 9:00 - 20:00</p>

                    <div class="phone mt-40">
                        <i class="icon_phone"></i>
                        <h3>Phone</h3>
                        <p>+1 888 5146 3269</p>
                    </div>                      

                    <div class="email mt-40">
                        <i class="icon_mail"></i>
                        <h3>Email</h3>
                        <a href="mailto:spam@unika.com">spam@unika.com</a>
                    </div>                      

                </div> 
            </div> 

        </div> 
    </div> 
</section> 