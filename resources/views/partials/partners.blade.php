<section class="section-wrap bg-light partners-light">
    <div class="container">
        <div class="row">

            <div id="owl-partners" class="owl-carousel owl-theme">

                <div class="item">
                    <a href="#">
                        <img src="upload/partner_logo_dark_1.png" alt="">
                    </a>
                </div> 

                <div class="item">
                    <a href="#">
                        <img src="upload/partner_logo_dark_2.png" alt="">
                    </a>
                </div> 

                <div class="item">
                    <a href="#">
                        <img src="upload/partner_logo_dark_3.png" alt="">
                    </a>
                </div> 

                <div class="item">
                    <a href="#">
                        <img src="upload/partner_logo_dark_4.png" alt="">
                    </a>
                </div> 

                <div class="item">
                    <a href="#">
                        <img src="upload/partner_logo_dark_5.png" alt="">
                    </a>
                </div> 

                <div class="item">
                    <a href="#">
                        <img src="upload/partner_logo_dark_6.png" alt="">
                    </a>
                </div> 

                <div class="item">
                    <a href="#">
                        <img src="upload/partner_logo_dark_1.png" alt="">
                    </a>
                </div> 

                <div class="item">
                    <a href="#">
                        <img src="upload/partner_logo_dark_2.png" alt="">
                    </a>
                </div> 

            </div> 
            
        </div> 
    </div> 
</section> 