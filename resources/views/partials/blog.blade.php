<section class="section-wrap bg-light" id="from-blog">
    <div class="container">         
        <h2 class="text-center">From Blog</h2>
        <p class="subheading text-center">Our Updated News</p>

        <div class="row mt-20">

            <div class="col-md-4 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.2s">
                <div class="blog-col-3 mt-30">
                    <div class="entry-box">
                        <div class="entry-img">
                            <a href="blog-single.html">
                                <img src="upload/blog_1.jpg" alt="">
                            </a>
                        </div>
                        <div class="entry-title">
                            <h4><a href="blog-single.html">Why we make awesome projects</a></h4>
                        </div>
                        <ul class="entry-meta">
                            <li>April 26, 2015</li>
                            <li>
                                <a href="blog-single.html" class="transition"> Web-Design</a>
                            </li>                                       
                        </ul>
                        <div class="entry-content">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi.</p>
                            <a href="blog-single.html" class="btn btn-small btn-rounded">Read More</a>
                            <a href="blog-single.html" class="blog-comments right transition">15</a>
                        </div>
                    </div> 
                </div> 
            </div> 

            <div class="col-md-4 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.4s">
                <div class="blog-col-3 mt-30">
                    <div class="entry-box">
                        <div class="entry-img">
                            <a href="blog-single.html">
                                <img src="upload/blog_2.jpg" alt="">
                            </a>
                        </div>
                        <div class="entry-title">
                            <h4><a href="blog-single.html">Why we make awesome projects</a></h4>
                        </div>
                        <ul class="entry-meta">
                            <li>April 26, 2015</li>
                            <li>
                                <a href="blog-single.html" class="transition"> Web-Design</a>
                            </li>                                       
                        </ul>
                        <div class="entry-content">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi.</p>
                            <a href="blog-single.html" class="btn btn-small btn-rounded">Read More</a>
                            <a href="blog-single.html" class="blog-comments right transition">15</a>
                        </div>
                    </div> 
                </div> 
            </div> 

            <div class="col-md-4 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s">
                <div class="blog-col-3 mt-30">
                    <div class="entry-box">
                        <div class="entry-slider">
                            <div id="owl-slider-small-img" class="owl-carousel owl-theme">

                                <div class="item">
                                    <a href="#">
                                        <img src="upload/blog_gallery_1.jpg" alt="">
                                    </a>
                                </div> 

                                <div class="item">
                                    <a href="#">
                                        <img src="upload/blog_gallery_2.jpg" alt="">
                                    </a>
                                </div> 

                                <div class="item">
                                    <a href="#">
                                        <img src="upload/blog_gallery_3.jpg" alt="">
                                    </a>
                                </div> 

                            </div> 
                        </div>
                        <div class="entry-title">
                            <h4><a href="blog-single.html">Why we make awesome projects</a></h4>
                        </div>
                        <ul class="entry-meta">
                            <li>April 26, 2015</li>
                            <li>
                                <a href="blog-single.html" class="transition"> Web-Design</a>
                            </li>                                       
                        </ul>
                        <div class="entry-content">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi.</p>
                            <a href="blog-single.html" class="btn btn-small btn-rounded">Read More</a>
                            <a href="blog-single.html" class="blog-comments right transition">15</a>
                        </div>
                    </div> 
                </div> 
            </div> 

        </div> 
    </div> 
</section> 