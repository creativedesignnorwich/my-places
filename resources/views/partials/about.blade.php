<section class="about-us-intro" id="about-us">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-6 nopadding oh">
                <img src="/slide3.jpg" alt="">    
            </div> 

            <div class="col-md-6">
                <div class="about-features">

                    <div class="col-md-12">
                        <h2>Why We Are The Best</h2>
                        <p class="subheading">
                            We Are Always Provide Best Quality and Professinal level of features.<br>
                            Unika is Super Awesome Material Design Theme.
                        </p>
                    </div> 

                    <div class="col-md-12 col-lg-5 wow bounceInLeft" data-wow-duration="1s" data-wow-delay="0.1s">
                        <div class="about-feature mt-50 mt-lrg-10 mt-mdm-30">
                            <h4>Drag &amp; Drop Builder</h4>
                            <p>Unika is a simple and elegant template with tons of features. Lorem ipsum dolor sit amet, consectetur.</p>
                        </div>
                    </div>

                    <div class="col-md-12 col-lg-5 col-lg-offset-1 wow bounceInRight" data-wow-duration="1s" data-wow-delay="0.1s">
                        <div class="about-feature mt-50 mt-lrg-10 mt-mdm-30">
                            <h4>2 minutes to install</h4>
                            <p>Unika is a simple and elegant template with tons of features. Lorem ipsum dolor sit amet, consectetur.</p>
                        </div>
                    </div>

                    <div class="col-md-12 col-lg-5 wow bounceInLeft" data-wow-duration="1s" data-wow-delay="0.1s">
                        <div class="about-feature mt-50 mt-lrg-10 mt-mdm-30">
                            <h4>Creative Design</h4>
                            <p>Unika is a simple and elegant template with tons of features. Lorem ipsum dolor sit amet, consectetur.</p>
                        </div>
                    </div>

                    <div class="col-md-12 col-lg-5 col-lg-offset-1 wow bounceInRight" data-wow-duration="1s" data-wow-delay="0.1s">
                        <div class="about-feature mt-50 mt-lrg-10 mt-mdm-30">
                            <h4>Fully Responsive</h4>
                            <p>Unika is a simple and elegant template with tons of features. Lorem ipsum dolor sit amet, consectetur.</p>
                        </div>
                    </div>

                </div>                      
            </div> 
            
        </div> 
    </div> 
</section> 