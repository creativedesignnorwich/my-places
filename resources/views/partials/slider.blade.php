<section>               
    <div class="rev_slider_wrapper">
        <div class="rev_slider" id="slider1" data-version="5.0">
            <ul>

                <li data-transition="fade"
                    data-easein="default" 
                    data-easeout="default"
                    data-slotamount="1"
                    data-masterspeed="1200"
                    data-delay="7000"
                    >

                    <img src="/slide4.jpg"
                        alt="slidebg1"
                        data-kenburns="on"
                        data-bgposition="left center"
                        data-bgpositionend="right center"
                        data-duration="36000"
                        data-scalestart="100"
                        data-scaleend="150"
                        data-bgparallax="10"
                        class="rev-slidebg"                                 
                        >

                    <div class="tp-caption hero-text rs-parallaxlevel-10"
                        data-x="center"
                        data-y="center"
                        data-transform_idle="o:1;s:1000;"
                        data-transform_in=";z:0;rX:0;rY:0;rZ:0;sX:0.7;sY:0.7;skX:0;skY:0;opacity:0;s:1000;e:Power3.easeOut;" 
                        data-transform_out="opacity:0;sX:0.7;sY:0.7;s:1000;e:Power3.easeInOut;"
                        data-start="1200">Smarter Property<br> Management
                    </div>

                    <div class="tp-caption medium_text rs-parallaxlevel-10"
                        data-x="center"
                        data-y="center"
                        data-voffset="80"
                        data-transform_idle="o:1;s:1000;"
                        data-transform_in="y:100;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1000;e:Power3.easeOut;" 
                        data-transform_out="opacity:0;sX:0.7;sY:0.7;s:1000;e:Power3.easeInOut;"
                        data-start="1400">
                    </div>
        
                </li>

                <!-- <li data-transition="fade"
                    data-easein="default" 
                    data-easeout="default"
                    data-slotamount="1"
                    data-masterspeed="1200"
                    data-delay="7000"
                    >
                    
                    <img src="/slide3.jpg"
                        alt="slidebg1"
                        data-kenburns="on"
                        data-bgposition="left center"
                        data-bgpositionend="right center"
                        data-duration="36000"
                        data-scalestart="100"
                        data-scaleend="150"
                        data-bgparallax="10"                                
                        class="rev-slidebg"
                        >

                    <div class="tp-caption hero-text giant-nocaps rs-parallaxlevel-10"
                        data-x="center"
                        data-y="center"
                        data-transform_idle="o:1;s:1000;"
                        data-transform_in="y:[100%];z:0;sX:1;sY:1;skX:0;skY:0;s:1000;e:Power3.easeInOut;" 
                        data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;" 
                        data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                        data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                        data-lineheight="['100','100','100','100']"
                        data-start="1200">Manage All Your<br> Properties Online
                    </div>

                    <div class="tp-caption medium_text rs-parallaxlevel-10"
                        data-x="center"
                        data-y="center"
                        data-voffset="80"
                        data-transform_idle="o:1;s:1000;"
                        data-transform_in="y:100;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1000;e:Power3.easeOut;" 
                        data-transform_out="opacity:0;sX:0.7;sY:0.7;s:1000;e:Power3.easeInOut;"
                        data-start="1400">
                    </div>
        
                </li> -->

            </ul>
            <div class="local-scroll">
                <a href="#intro" class="scroll-down">
                    <i class="arrow_carrot-down"></i>
                </a>
            </div>

        </div>
    </div>
</section>