<section class="call-to-action">
    <div class="container">         
        <h2 class="text-center color-white">Have a question? Feel Free To Contact Us</h2>
        <div class="text-center mt-40">
            <a href="#contact" class="btn btn-large btn-light-solid">Contact Us</a>
        </div>
    </div> 
</section> 