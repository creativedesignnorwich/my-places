<header id="home">
    <div class="main-nav" role="navigation">

        <nav class="nav-hide">
            <ul class="nav local-scroll">
                <li class="active"><a href="#home">Home</a></li>
                <li><a href="#portfolio-masonry">Works</a></li>
                <li><a href="#about-us">About Us</a></li>
                <li><a href="#services">Services</a></li>
                <li><a href="#from-blog">Blog</a></li>
                <li><a href="#contact">Contact</a></li>
                @if (Auth::check())
                    <li><a href="/admin/dashboard">Dashboard</a></li>
                    <li><a href="/logout">Logout</a></li>
                @else 
                    <li><a href="/login">Login</a></li>
                    <li><a href="/register">Register</a></li>
                @endif
            </ul>
        </nav>

        <div class="container clearfix">

            <div class="logo-light local-scroll">
                <a href="#home" class="logo">
                    <img src="upload/logo-light.png" alt="logo">
                </a>
            </div>

            <div class="navbar-toggle">
                <div class="bar1"></div>
                <div class="bar2"></div>
                <div class="bar3"></div>
            </div>

        </div>
    </div>
</header>